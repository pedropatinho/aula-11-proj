package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }

}